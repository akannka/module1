#include <iostream>
#include <string>
//ex 3
/*
int main() {
    int result = 0;
    int min1, min2;
    int input_count = 0;

    while (result <= 50){
        int value;
        std::cout << "Enter number";
        std::cin  >> value;
        result += value;

        if (input_count == 0){
            min1 = value;
        }else if (input_count == 1){
            min2 = value;
            if (min1 > min2){
                std::swap(min1, min2);
            }
;       }
        if (value < min2){
            std::swap(value, min2);
        }
        if (value < min1){
            std::swap(value, min1);
        }

        input_count++;
    }
    std::cout << "min value:" << std::endl;
    std::cout << min1 << std::endl;
    if (input_count > 1){
        std::cout << min2 << std::endl;
    }
    return 0;
}
*/
// ex4
class Bird{
public:
    virtual void sing() const = 0;
    Bird (std :: string song) : song(std:: move(song)){}
protected:
    std :: string song;
};

class Nightingale : public Bird{
public:
    using Bird :: Bird;
    void sing() const override{
        std :: cout << this->song << std:: endl;
    }
};

class Sparrow : public Bird{
public:
    using Bird :: Bird;
    void sing() const override{
        auto len = std::min(3, (int)this->song.size());
        for (int i = 0; i < len; i++){
            std::cout << this ->song[i];
        }
        std::cout << std::endl;
    }
};

int main() {
    Nightingale bird1("Nightingale song ... ");
    Sparrow bird2 ("Sparrow song ... ");

    bird1.sing();
    bird2.sing();
    return 0;
}